import * as React from 'react'

import './style.scss'

const ValidInfo = () => (
  <table className="valide">
    <tbody>
      <tr>
        <td>
          <b>Chaînes Invalides</b>
          <br /> - qui ne parle pas ou peu français,
          <br /> - faites par des marques, par la télévision
          <br /> - les groupes musicaux (style Vévo),
          <br /> - les vides, qui spamment, qui reuploadent des films ou autres.
        </td>
        <td>
          <b>Chaînes Valides</b>
          <br /> - qui parle français.
          <br /> - faites par des "Youtubeurs" (pro ou amateurs).
        </td>
      </tr>
    </tbody>
  </table>
)

export default ValidInfo

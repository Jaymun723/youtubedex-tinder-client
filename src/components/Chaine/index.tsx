import * as React from 'react'

import './style.scss'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'
import { GetChannelSnippet, GetChannelSnippetVariables } from './__generated__/GetChannelSnippet'

import Loading from '../../media/loading.gif'

interface BoneProps {
  imgSrc: string
  url: string
  name: string
  description: string
}

export class Bone extends React.Component<BoneProps> {
  render() {
    return (
      <div className="chaines">
        <div className="chaine">
          <img src={this.props.imgSrc} />
          <h1>
            <a href={this.props.url} target="_blank">
              {this.props.name}
            </a>
          </h1>
          <p className="info-chaine">{this.props.description}</p>
        </div>
      </div>
    )
  }
}

interface ChaineProps {
  urlId: string
  urlPseudo?: string
}

const getChannelSnippet = gql`
  query GetChannelSnippet($id: String!) {
    getChannelSnippet(id: $id) {
      title
      thumbnails
      description
    }
  }
`

const Chaine = (props: ChaineProps) => {
  const id = props.urlId.replace('http://youtube.com/channel/', '')
  return (
    <Query<GetChannelSnippet, GetChannelSnippetVariables> query={getChannelSnippet} variables={{ id }}>
      {({ data, loading, error }) => {
        if (error) {
          return <div>Error</div>
        }
        if (loading || !data) {
          return <Bone imgSrc={Loading} url="#" name="" description="" />
        }
        return (
          <Bone
            imgSrc={data.getChannelSnippet.thumbnails}
            url={props.urlPseudo ? props.urlPseudo : props.urlId}
            name={data.getChannelSnippet.title}
            description={data.getChannelSnippet.description}
          />
        )
      }}
    </Query>
  )
}

export default Chaine

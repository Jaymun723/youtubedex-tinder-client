/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetChannelSnippet
// ====================================================

export interface GetChannelSnippet_getChannelSnippet {
  title: string
  thumbnails: string
  description: string
}

export interface GetChannelSnippet {
  getChannelSnippet: GetChannelSnippet_getChannelSnippet // Retourne des informations sur une Chaine Youtube.  _Relatif à l'api de Youtube_
}

export interface GetChannelSnippetVariables {
  id: string
}

//==============================================================
// START Enums and Input Objects
// All enums and input objects are included in every output file
// for now, but this will be changed soon.
// TODO: Link to issue to fix this.
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================

import ApolloClient from 'apollo-boost'
import { graphlUrl, code } from '../../config'

export const client = new ApolloClient({
  uri: graphlUrl,
  request: async (opeartion) => {
    opeartion.setContext({
      headers: {
        authorization: code,
      },
    })
  },
})

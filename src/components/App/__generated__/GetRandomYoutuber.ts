/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetRandomYoutuber
// ====================================================

export interface GetRandomYoutuber_getRandomYoutuber {
  URLPSEUDO: string | null
  URLID: string
  uuid: string // UUID de youtuber dans la base de données
}

export interface GetRandomYoutuber {
  getRandomYoutuber: GetRandomYoutuber_getRandomYoutuber // Retourne un youtuber non validé au hasard.  **Requière un header 'Authorization' correct**  _Relatif à l'api de YoutubeDex_
}

//==============================================================
// START Enums and Input Objects
// All enums and input objects are included in every output file
// for now, but this will be changed soon.
// TODO: Link to issue to fix this.
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================

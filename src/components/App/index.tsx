import * as React from 'react'
import { ApolloProvider } from 'react-apollo'

import { client } from '../Apollo/ApolloClient'

import { ValidContextProvider } from '../ValidButtons/context'

import ValidInfo from '../ValidInfo'
import { Content } from './Content'

import './style.scss'

export class App extends React.Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <ValidContextProvider>
          <ValidInfo />
          <Content />
        </ValidContextProvider>
      </ApolloProvider>
    )
  }
}

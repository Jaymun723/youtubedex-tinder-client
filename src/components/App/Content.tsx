import * as React from 'react'
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost'

import { GetRandomYoutuber } from './__generated__/GetRandomYoutuber'

import ValidButtons from '../ValidButtons'
import Categories from '../Categories'
import Chaine, { Bone as ChaineBone } from '../Chaine'
import Videos, { VideosBone, VideoBone } from '../Videos'

import Loading from '../../media/loading.gif'

const getRandomYoutuber = gql`
  query GetRandomYoutuber {
    getRandomYoutuber {
      URLPSEUDO
      URLID
      uuid
    }
  }
`

export const Content = () => (
  <Query<GetRandomYoutuber> query={getRandomYoutuber}>
    {({ data, loading, error }) => {
      if (error) {
        if (error.message === 'GraphQL error: No more youtubers to validate !') {
          return <div className="info">Plus de youtuber à valider !</div>
        } else {
          return (
            <div className="error">
              Une erreur dans la <code>query GetRandomYoutuber</code> est apparue
            </div>
          )
        }
      }
      if (loading || !data) {
        return (
          <>
            <ChaineBone imgSrc={Loading} description="" name="" url="#" />
            <VideosBone>
              {new Array(10).map(() => <VideoBone url="" vues={0} imgUrl={Loading} time="" title="" />)}
            </VideosBone>
          </>
        )
      }

      data.getRandomYoutuber.URLPSEUDO

      return (
        <>
          <Chaine urlId={data.getRandomYoutuber.URLID} />
          <Videos urlId={data.getRandomYoutuber.URLID} />
          <ValidButtons UUID={data.getRandomYoutuber.uuid} />
          <Categories uuid={data.getRandomYoutuber.uuid} />
        </>
      )
    }}
  </Query>
)

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetChannelVideos
// ====================================================

export interface GetChannelVideos_getChannelVideos {
  publishedAt: string
  title: string
  thumbnails: string
  url: string
  id: string
  views: number
}

export interface GetChannelVideos {
  getChannelVideos: GetChannelVideos_getChannelVideos[] // Retourne les 10 dernières vidéos d'une Chaine Youtube.  _Relatif à l'api de Youtube_
}

export interface GetChannelVideosVariables {
  id: string
}

//==============================================================
// START Enums and Input Objects
// All enums and input objects are included in every output file
// for now, but this will be changed soon.
// TODO: Link to issue to fix this.
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================

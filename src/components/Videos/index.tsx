import * as React from 'react'

import './style.scss'
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'
import { GetChannelVideosVariables, GetChannelVideos } from './__generated__/GetChannelVideos'
import { getTimeDif } from '../../utils'

import Loading from '../../media/loading.gif'

export class VideosBone extends React.Component {
  render() {
    return <div className="videos">{this.props.children}</div>
  }
}

interface VideoProps {
  imgUrl: string
  title: string
  vues: number
  time: string
  url: string
}

export const VideoBone = ({ imgUrl, title, time, vues, url }: VideoProps) => (
  <div className="video">
    <img src={imgUrl} alt={title} />
    <a href={url} target="_blank">
      {title}
    </a>
    <div className="info-video">
      {vues} vues, il y a {getTimeDif(time)}
    </div>
  </div>
)

const getChannelVideos = gql`
  query GetChannelVideos($id: String!) {
    getChannelVideos(id: $id) {
      publishedAt
      title
      thumbnails
      url
      id
      views
    }
  }
`

const Videos = (props: { urlId: string }) => {
  const id = props.urlId.replace('http://youtube.com/channel/', '')
  return (
    <Query<GetChannelVideos, GetChannelVideosVariables> query={getChannelVideos} variables={{ id }}>
      {({ data, loading, error }) => {
        if (error) {
          return <div className="error">{error.message}</div>
        }
        if (loading || !data) {
          let test = new Array(10)
          return (
            <VideosBone>
              {test.map((_, i) => <VideoBone title="" imgUrl={Loading} url="#" vues={0} time="" key={i} />)}
            </VideosBone>
          )
        }

        let vids = data.getChannelVideos

        return (
          <VideosBone>
            {vids.map((vid) => (
              <VideoBone
                title={vid.title}
                imgUrl={vid.thumbnails}
                url={vid.url}
                vues={vid.views}
                time={vid.publishedAt}
                key={vid.id}
              />
            ))}
          </VideosBone>
        )
      }}
    </Query>
  )
}

export default Videos

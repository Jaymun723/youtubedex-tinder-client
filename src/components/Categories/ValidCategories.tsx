import * as React from 'react'
import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'
import { GetCategories } from './__generated__/GetCategories'

const getCategories = gql`
  query GetCategories {
    getCategories {
      ID
      NAME
    }
  }
`

interface CategoriesSelectProps {
  name: string
  inputOnChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  inputValue: string
  selectOnChange: (e: React.ChangeEvent<HTMLSelectElement>) => void
  selectValue: string
}

export class CategoriesSelect extends React.Component<CategoriesSelectProps> {
  render() {
    return (
      <tr>
        <td>
          <strong>{this.props.name}</strong>
        </td>
        <td>
          <Query<GetCategories> query={getCategories}>
            {({ data, loading, error }) => {
              if (error) {
                return <div>Error</div>
              }
              if (loading || !data) {
                return <div>Loading</div>
              }
              return (
                <select
                  name={`select-${this.props.name}`}
                  value={this.props.selectValue}
                  onChange={this.props.selectOnChange}
                >
                  {data.getCategories.map((categorie) => (
                    <option key={categorie.ID} value={categorie.ID}>
                      {categorie.NAME}
                    </option>
                  ))}
                </select>
              )
            }}
          </Query>
        </td>
        <td>
          <strong>Sous-catégorie</strong>
        </td>
        <td>
          <input type="text" onChange={this.props.inputOnChange} value={this.props.inputValue} />
        </td>
      </tr>
    )
  }
}

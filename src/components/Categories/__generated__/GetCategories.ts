/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GetCategories
// ====================================================

export interface GetCategories_getCategories {
  ID: number // ID de la catégorie dans la base de données (ex: 1)
  NAME: string
}

export interface GetCategories {
  getCategories: GetCategories_getCategories[] // **Requière un header 'Authorization' correct**  _Relatif à l'api de YoutubeDex_
}

//==============================================================
// START Enums and Input Objects
// All enums and input objects are included in every output file
// for now, but this will be changed soon.
// TODO: Link to issue to fix this.
//==============================================================

//==============================================================
// END Enums and Input Objects
//==============================================================

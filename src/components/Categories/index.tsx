import * as React from 'react'

import './style.scss'
import { ValidContextConsumer } from '../ValidButtons/context'
import ValideCategories from './WrapperCategorie'

class CategoriesBone extends React.Component {
  render() {
    return <div className="categories">{this.props.children}</div>
  }
}

const Categories = (props: { uuid: string }) => (
  <CategoriesBone>
    <ValidContextConsumer>
      {({ cstate }) => {
        switch (cstate) {
          case 0:
            return null
          case 1:
            return <ValideCategories uuid={props.uuid} />
        }
      }}
    </ValidContextConsumer>
  </CategoriesBone>
)

export default Categories

// export default () => <ValidContextConsumer>{({ cstate }) => <div>{String(cstate)}</div>}</ValidContextConsumer>

import * as React from 'react'

import { gql, ApolloClient } from 'apollo-boost'

import { ApolloConsumer } from 'react-apollo'

import { CategoriesSelect } from './ValidCategories'

interface Props {
  client: ApolloClient<any>
  uuid: string
}

class CategoriesWrapper extends React.Component<Props> {
  state = {
    showCat: false,
    categorie1: {
      input: '',
      select: '',
    },
    categorie2: {
      input: '',
      select: '',
    },
    female: false,
    error: '',
  }

  handleInput = (name: 'categorie1' | 'categorie2') => (e: React.ChangeEvent<HTMLInputElement>) => {
    if (name === 'categorie1') {
      this.setState({ categorie1: { input: e.target.value, select: this.state.categorie1.select } })
    } else if (name === 'categorie2') {
      this.setState({ categorie2: { input: e.target.value, select: this.state.categorie2.select } })
    }
  }

  handleSelect = (name: 'categorie1' | 'categorie2') => (e: React.ChangeEvent<HTMLSelectElement>) => {
    // console.log(e.target.selectedOptions[0].value)
    if (name === 'categorie1') {
      this.setState({ categorie1: { input: this.state.categorie1.input, select: e.target.selectedOptions[0].value } })
    } else if (name === 'categorie2') {
      this.setState({ categorie2: { input: this.state.categorie2.input, select: e.target.selectedOptions[0].value } })
    }
  }

  handleSubmit = () => {
    if (this.state.categorie1.input === '' || this.state.categorie2.input === '') {
      this.setState({ error: 'Les sous catégories ne sont pas définies...' })
    } else if (this.state.categorie1.input === this.state.categorie2.input) {
      this.setState({ error: 'Les sous catégories sont les mêmes...' })
    } else if (this.state.categorie1.select === '' || this.state.categorie2.select === '') {
      this.setState({ error: 'Les catégories ne sont pas définies...' })
    } else if (this.state.categorie1.select === this.state.categorie2.select) {
      this.setState({ error: 'Les catégories sont les mêmes...' })
    } else {
      this.setState({ error: '' })
      this.props.client
        .mutate({
          mutation: validateYoutuber,
          variables: {
            input: {
              uuid: this.props.uuid,
              VALIDATE: true,
              CAT1: Number(this.state.categorie1.select),
              CAT2: Number(this.state.categorie2.select),
              SOUSCAT1: this.state.categorie1.input,
              SOUSCAT2: this.state.categorie2.input,
              FEMALE: this.state.female,
            },
          },
        })
        .then(() => {
          location.reload()
        })
    }
  }

  render() {
    return (
      <div className="categories">
        <table>
          <tbody>
            <CategoriesSelect
              name="Catégorie 1"
              inputOnChange={this.handleInput('categorie1')}
              inputValue={this.state.categorie1.input}
              selectOnChange={this.handleSelect('categorie1')}
              selectValue={this.state.categorie1.select}
            />
            <CategoriesSelect
              name="Catégorie 2"
              inputOnChange={this.handleInput('categorie2')}
              inputValue={this.state.categorie2.input}
              selectOnChange={this.handleSelect('categorie2')}
              selectValue={this.state.categorie2.select}
            />
            <tr>
              <td colSpan={4}>
                <input
                  id="checkBox"
                  type="checkbox"
                  checked={this.state.female}
                  onClick={() => this.setState({ female: !this.state.female })}
                />{' '}
                Chaîne tenue par une/des filles
              </td>
            </tr>
          </tbody>
        </table>
        <span className="showcat" onClick={() => this.setState({ showCat: !this.state.showCat })}>
          Voir des examples de sous-catégories
        </span>
        {this.state.showCat ? (
          <div id="myDIV">
            <div className="examples-cat">
              Afrique, Animaux, Armes et Explosifs, Asian Pop, Au Féminin, Au Masculin, Auto Moto, Bateaux, Bébés,
              Bricolage, Business, Chasse, Pêche et Nature, Christianisme, Chroniques Jeux-Vidéo, Clash Royal/Clans,
              Combat, Cosplay, Covers, Créatif, Culture G, Danse et Gymnastique, Dessin, Dessin-Animés, Disney,
              Divertissement, Dofus, Dragon Ball, Droite, Economie, En Couple, Equitation, Féminisme, FIFA, Football,
              FPS, Gaming, Gauche, Geek, Graphisme, GTA, Guitare, Harry Potter, Hearthstone, Histoire, Hypnose,
              Informatique, Islam, Jardin et Agriculture, Jeux de société, Jouets, Juridique, Langues, League of
              Legends, Lifestyle, Linguistique, Littérature, Live, Magie, Marvel et DC, Medecine, Mercerie, Minecraft,
              Multi-Gaming, Musculation et Fitness, Musicologie, NBA, Neutre, One Piece, Overwatch, Parkour, Parodie,
              Permis de conduire, Philosophie, Photographie, Poésie, Pokémon, Psychologie, Rap, Reggae, Retro, Ride,
              Skate et Ski, Santé et Zen, Scolarité, Sexualité, Simpsons, Sims, Simulator, Sketchs, Skyrim, Sport, Sport
              Extrême, Star Wars, Starcraft, Tatouages, Test et Énigmes, Tir, Vegan, Vélo et BMX, Vidéo-Making, Voyage,
              Walking Dead, World of Warcraft, World of Warship, Yoga, Zapping et Compilation
            </div>
          </div>
        ) : null}
        <div className="btn">
          {this.state.error !== '' ? <div className="error">{this.state.error}</div> : null}
          <div className="btn-valider" onClick={this.handleSubmit}>
            Valider
          </div>
        </div>
      </div>
    )
  }
}

const validateYoutuber = gql`
  mutation ValidateYoutuber($input: ValideInput!) {
    validateYoutuber(input: $input) {
      uuid
    }
  }
`

const Wrapper = (props: { uuid: string }) => (
  <ApolloConsumer>
    {(client) => {
      return <CategoriesWrapper client={client} uuid={props.uuid} />
    }}
  </ApolloConsumer>
)

export default Wrapper

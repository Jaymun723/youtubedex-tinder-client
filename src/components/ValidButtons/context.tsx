import * as React from 'react'

// 0 pas vu
// 1 valide
// 2 pas valide

const ValidContext = React.createContext({ valid: () => {}, cstate: 0 })
export const ValidContextConsumer = ValidContext.Consumer
export class ValidContextProvider extends React.Component {
  state = {
    cstate: 0,
    valid: () => this.setState({ cstate: 1 }),
  }

  render() {
    return <ValidContext.Provider value={this.state}>{this.props.children}</ValidContext.Provider>
  }
}

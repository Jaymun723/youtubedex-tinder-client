import * as React from 'react'

import './style.scss'
import { ValidContextConsumer } from './context'
import { gql } from 'apollo-boost'
import { Mutation } from 'react-apollo'
import { InvalidateYoutuber, InvalidateYoutuberVariables } from './__generated__/InvalidateYoutuber'

const invalidateYoutuber = gql`
  mutation InvalidateYoutuber($input: ValideInput!) {
    validateYoutuber(input: $input) {
      uuid
    }
  }
`

interface Props {
  UUID: string
}

const ValideButtons = (props: Props) => (
  <Mutation<InvalidateYoutuber, InvalidateYoutuberVariables>
    mutation={invalidateYoutuber}
    variables={{ input: { uuid: props.UUID, VALIDATE: false } }}
  >
    {(mutate) => {
      const onClick = async (e: React.MouseEvent<HTMLDivElement>) => {
        mutate()
        location.reload()
      }
      return (
        <ValidContextConsumer>
          {({ valid }) => (
            <div className="btn">
              <div className="btn-invalider" onClick={onClick}>
                Invalider
              </div>
              <div className="btn-valider" onClick={valid}>
                Valider
              </div>
            </div>
          )}
        </ValidContextConsumer>
      )
    }}
  </Mutation>
)

export default ValideButtons

/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: InvalidateYoutuber
// ====================================================

export interface InvalidateYoutuber_validateYoutuber {
  uuid: string // UUID de youtuber dans la base de données
}

export interface InvalidateYoutuber {
  validateYoutuber: InvalidateYoutuber_validateYoutuber // **Requière un header 'Authorization' correct**  _Relatif à l'api de YoutubeDex_
}

export interface InvalidateYoutuberVariables {
  input: ValideInput
}

//==============================================================
// START Enums and Input Objects
// All enums and input objects are included in every output file
// for now, but this will be changed soon.
// TODO: Link to issue to fix this.
//==============================================================

// 'Input' pour valider ou pas un youtuber.
// _Relatif à l'api de YoutubeDex_
interface ValideInput {
  uuid: string
  VALIDATE: boolean
  CAT1?: number | null
  SOUSCAT1?: string | null
  CAT2?: number | null
  SOUSCAT2?: string | null
  FEMALE?: boolean | null
}

//==============================================================
// END Enums and Input Objects
//==============================================================

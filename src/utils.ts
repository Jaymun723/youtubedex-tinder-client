export function getTimeDif(time: string) {
  const now = new Date()
  const date = new Date(time)

  let delta = Math.abs(now.getTime() - date.getTime()) / 1000

  const years = Math.floor(delta / 30758400)
  delta -= years * 30758400

  const month = Math.floor(delta / 2592000)
  delta -= month * 2592000

  const days = Math.floor(delta / 86400)
  delta -= days * 86400

  const hours = Math.floor(delta / 3600)
  delta -= hours * 3600

  const minutes = Math.floor(delta / 60)
  delta -= minutes * 60

  const seconds = Math.floor(delta % 60)

  // let result = {}
  if (years) {
    return `${years} an${years > 1 ? 's' : ''}`
  } else if (month) {
    return `${month} mois`
  } else if (days) {
    return `${days} jour${days > 1 ? 's' : ''}`
  } else if (hours) {
    return `${hours} heure${hours > 1 ? 's' : ''}`
  } else if (minutes) {
    return `${minutes} minutes${minutes > 1 ? 's' : ''}`
  } else {
    return `${seconds} seconde${seconds > 1 ? 's' : ''}`
  }
}
